+++
title = "About"
type = "about"
weight = 10
+++

We have a long and varied experience within prefabrication and welding, steel constructioning, pipes for on and offshore, natural gas pipelines, plumbing, sheet metal works and all mechanical and hydraulic components.

We are personally involved in each project and combine skills with the upmost quality workmanship. Certified by Veritas DNV - Norwegian Classification, our skilled operators provides the best with highest quality results.

We can custom design any steel job to match the specifications.

![about](../images/mac.jpg)

