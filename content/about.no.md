+++
title = "Om Oss"
type = "about"
weight = 10
+++

Vi har en lang og variert erfaring innen prefabrikering og sveising, stålkonstruksjon, rør for både på og offshore, naturgassrørledninger, rørleggerarbeid, metallverk og alle mekaniske og hydrauliske komponenter.
Vi er personlig involvert i hvert prosjekt og kombinerer ferdigheter med det høyeste kvalitetsarbeidet. Sertifisert av Veritas DNV - Norsk Klassifisering, gir våre dyktige operatører det beste med høyest kvalitetsresultat.
Vi kan tilpasse design til enhver ståljobb for å matche spesifikasjonene.