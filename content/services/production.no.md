---
title: "Produksjon"
description: "We have 20+ years of expearence with production, installation and maintanence in food
industry."
weight: 20
---
We have a long and varied experience within prefabrication and welding, steel constructioning, pipes for on/offshore, natural gas pipelines, plumbing, sheet metal works and all mechanical and hydraulic components.",
        "We are personally involved in each project and combine skills with the upmost quality workmanship. Certified by Veritas DNV - Norwegian Classification, our skilled operators provides the best with highest quality results.",
        "We can custom design any steel job to match the specifications at hand. 


<!--more-->

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
